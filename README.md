#**Connection with a Price**

>"Every connection has its price; the only thing you can be sure of is that sooner or later you'll have to pay."

**Steven Shaviro** gives us this quote. It's something that I think humans don't generally like to think about, but it rings true for a multitude of reasons. 

  + Many practices we were once able to  complete independently have been long forgotten. 
  + Humans keep inventing new ways to kill ourselves with technology.
  + One day, technology will surpass our intentions and just kill us directly.


These reasons aren't always super obvious to many people. Something you may often hear (and abhor) is something along the lines of "*Back in my day...* "

![baby boomers](https://previews.123rf.com/images/kk5hy/kk5hy0707/kk5hy070700359/1267937-Man-furious-with-his-bad-stock-trades-taking-it-out-on-his-computer-by-shooting-at-it--Stock-Photo.jpg)

***********************************************************  

>Shaviro's point is that technology, no matter how far ahead it puts us, will always cost us in some way. The connections we foster with technology will always require a sacrifice of some sort.
**********************************************************
>As a millenial, I was raised on technology. The first reason doesn't matter much to me. Even the second doesn't make me raise my eyebrows very much. The third, however, I think about every day. I have talked about this before and I may seem obsessive about it, but the third reason isn't too far off and we should probably all start preparing ourselves for elimination via the Singularity.

[What is the Singularity?](https://en.wikipedia.org/wiki/Technological_singularity)

[Oh My God](http://apocalypticsurvivalguide.com/)